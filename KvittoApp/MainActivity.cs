﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace KvittoApp
{
	[Activity (Label = "KvittoApp", MainLauncher = true, Icon = "@drawable/icon", Theme = "@android:style/Theme.NoTitleBar")]
	public class MainActivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.Main);

			new ReceiptManager();

			Button receiptButton = FindViewById<Button> (Resource.Id.Main_AddReceipt);
			Button archiveButton = FindViewById<Button> (Resource.Id.Main_Archive);
			Button loginButton = FindViewById<Button> (Resource.Id.Main_LogIn);

			receiptButton.Click += openReceiptAdder;
			archiveButton.Click += openArchive;
			loginButton.Click += openLogin;
		}

		private void openReceiptAdder(object sender, EventArgs e)
		{
			Intent i = new Intent (this, typeof(ReceiptsActivity));
			StartActivity(i);
		}

		private void openArchive(object sender, EventArgs e)
		{
			Intent i = new Intent (this, typeof(ArchiveActivity));
			StartActivity(i);
		}

		private void openLogin(object sender, EventArgs e)
		{
			//Intent i = new Intent (this, typeof(Login));
			//StartActivity(i);
		}
	}
}


