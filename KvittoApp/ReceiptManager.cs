﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

using SQLite;

namespace KvittoApp
{
	public sealed class ReceiptManager
	{
		private static ReceiptManager Instance = null;
		private static readonly object padlock = new object();

		private string dbPath;
		public SQLiteConnection db;

		public ReceiptManager()
		{
			dbPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			dbPath += "\\database.db";
			db = new SQLiteConnection(dbPath);
			db.CreateTable<ReceiptEntry>();
			db.Close();
		}

		public static ReceiptManager getInstance
		{
			get
			{
				lock (padlock)
				{
					if (Instance == null)
					{
						Instance = new ReceiptManager();
					}
					return Instance;
				}
			}
		}

		public void addReceipt(ReceiptEntry r)
		{
			db = new SQLiteConnection(dbPath);
			db.Insert(r);
			db.Close();
		}

		public void deleteReceipt(ReceiptEntry r)
		{
			db = new SQLiteConnection(dbPath);
			db.Delete(r);
			db.Close();
		}

		public ReceiptEntry getReceipt(int id)
		{
			db = new SQLiteConnection(dbPath);
			IEnumerable<ReceiptEntry> getReceipt = db.Table<ReceiptEntry>().Where(r => r.Id == id);
			ReceiptEntry receipt = getReceipt.FirstOrDefault();
			db.Close();

			return receipt;
		}

		public List<ReceiptEntry> getList()
		{
			db = new SQLiteConnection(dbPath);
			IEnumerable<ReceiptEntry> receipts = db.Table<ReceiptEntry>();
			List<ReceiptEntry> list = receipts.Select (r => r).ToList();
			list = list.OrderByDescending(r => r.YearValue+"/"+r.MonthValue+"/"+r.DayValue).ToList();
			db.Close();

			return list;
		}

		public void updateDescription(int id, ReceiptEntry update)
		{
			db = new SQLiteConnection (dbPath);
			db.Update (update);
			db.Close();
		}
	}
}

