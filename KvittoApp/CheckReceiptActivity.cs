﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace KvittoApp
{
	[Activity (Label = "CheckReceiptActivity", Theme = "@android:style/Theme.NoTitleBar")]			
	public class CheckReceipt : Activity
	{
		int id;
		ReceiptEntry receipt;

		bool editMode = false;

		TextView numberText;
		TextView dateText;
		TextView nameText;
		TextView taxText;
		TextView nettoText;
		TextView totalText;
		TextView personText;

		TextView descText;
		EditText editDesc;
		Button editButton;
		Button cancelButton;

		ReceiptManager manager = ReceiptManager.getInstance;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.CheckReceipt);

			id = Intent.GetIntExtra("selectedId", -1);
			Console.WriteLine("Selected id: "+id);
			receipt = manager.getReceipt(id);

			numberText = FindViewById<TextView> (Resource.Id.Check_Header);
			dateText = FindViewById<TextView> (Resource.Id.Check_Date);
			nameText = FindViewById<TextView> (Resource.Id.Check_Name);
			taxText = FindViewById<TextView> (Resource.Id.Check_Tax);
			nettoText = FindViewById<TextView> (Resource.Id.Check_Netto);
			totalText = FindViewById<TextView> (Resource.Id.Check_Total);
			personText = FindViewById<TextView> (Resource.Id.Check_People);

			descText = FindViewById<TextView> (Resource.Id.Check_Description);
			editDesc = FindViewById<EditText> (Resource.Id.Check_DescriptionEdit);
			editButton = FindViewById<Button> (Resource.Id.Check_EditButton);
			cancelButton = FindViewById<Button> (Resource.Id.Check_CancelButton);

			editDesc.Visibility = ViewStates.Gone;
			cancelButton.Visibility = ViewStates.Gone;

			initText();

			editButton.Click += delegate
			{
				if (editMode)
				{
					setDescription(true);
				} else
				{
					startEdit();
				}
				editMode = !editMode;
			};

			cancelButton.Click += delegate
			{
				setDescription(false);
				editMode = !editMode;
			};
		}

		private void initText()
		{
			numberText.Text = ("Kvittonummer: " + receipt.NumberValue);
			dateText.Text = (receipt.YearValue + "-" + receipt.MonthValue + "-" + receipt.DayValue);
			nameText.Text = receipt.NameValue;
			taxText.Text = (receipt.TaxValue + "%");
			nettoText.Text = receipt.NettoValue.ToString();
			totalText.Text = receipt.TotalValue.ToString();
			personText.Text = receipt.PersonValue.ToString();

			descText.Text = receipt.DescriptionValue;
			editDesc.Text = receipt.DescriptionValue;
		}

		private void setDescription(bool enter)
		{
			if (enter) {
				receipt.DescriptionValue = editDesc.Text;
				manager.updateDescription (id, receipt);
				descText.Text = receipt.DescriptionValue;
			} else {
				editDesc.Text = descText.Text;
			}
				
			descText.Visibility = ViewStates.Visible;
			editDesc.Visibility = ViewStates.Gone;

			editButton.Text = "Edit Description";
			cancelButton.Visibility = ViewStates.Gone;
		}

		private void startEdit()
		{
			descText.Visibility = ViewStates.Invisible;
			editDesc.Visibility = ViewStates.Visible;
			editDesc.RequestFocus();

			editButton.Text = "Save";
			cancelButton.Visibility = ViewStates.Visible;
		}
	}
}

