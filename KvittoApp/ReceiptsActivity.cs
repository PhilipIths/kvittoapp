using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using SQLite;

namespace KvittoApp
{
	[Activity (Label = "ReceiptsActivity", Theme = "@android:style/Theme.NoTitleBar")]		
	public class ReceiptsActivity : Activity
	{	
		EditText yearEdit;
		EditText monthEdit;
		EditText dayEdit;
		EditText nameEdit;
		TableLayout table;
		TextView saldo;
		Button addRowButton;
		Button saveButton;

		ReceiptManager manager = ReceiptManager.getInstance;

		protected override void OnCreate (Bundle bundle)
		{
			// Initialize
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.Receipts);

			// EditTexts
			yearEdit = FindViewById<EditText> (Resource.Id.Add_YearEdit);
			monthEdit = FindViewById<EditText> (Resource.Id.Add_MonthEdit);
			dayEdit = FindViewById<EditText> (Resource.Id.Add_DayEdit);
			nameEdit = FindViewById<EditText> (Resource.Id.Add_NameEdit);

			// TableLayout
			table = FindViewById<TableLayout> (Resource.Id.Add_Table);

			//TextView
			saldo = FindViewById<TextView>(Resource.Id.Add_Saldo);

			// Buttons
			addRowButton = FindViewById<Button> (Resource.Id.Add_AddButton);
			saveButton = FindViewById<Button> (Resource.Id.Add_SaveButton);

			// Buttonclicks
			addRowButton.Click += delegate {
				newTableRow ();
				saldo.Text = "";
			};

			saveButton.Click += delegate {
				if (validateInput())
				{
					saveReceipts();
					clearView();
				}
			};

			newTableRow();
		}

		private void newTableRow()
		{
			RowInstance rowInfo = new RowInstance ();
			rowInfo.row = LayoutInflater.From (this).Inflate (Resource.Drawable.TableRowTemplate, null, false);
			table.AddView(rowInfo.row);

			Spinner thisTax = rowInfo.row.FindViewById<Spinner>(Resource.Id.Table_Tax);
			EditText thisTotal = rowInfo.row.FindViewById<EditText>(Resource.Id.Table_Total);
			Button deleteRow = rowInfo.row.FindViewById<Button> (Resource.Id.Table_DeleteButton);

			initTaxSpinner(thisTax);

			thisTax.ItemSelected += (object sender, AdapterView.ItemSelectedEventArgs e) => {
				updateText(rowInfo);
			};

			thisTotal.TextChanged += (object sender, Android.Text.TextChangedEventArgs e) => {
				updateText(rowInfo);
				updateSaldo();
			};

			deleteRow.Click += delegate
			{
				if (table.ChildCount > 1)
				{
					table.RemoveView(rowInfo.row);
					updateSaldo();
				}
			};
		}

		private void updateText(RowInstance rowInfo)
		{
			Spinner updateTax = rowInfo.row.FindViewById<Spinner>(Resource.Id.Table_Tax);
			TextView updateNetto = rowInfo.row.FindViewById<TextView>(Resource.Id.Table_Netto);
			EditText updateTotal = rowInfo.row.FindViewById<EditText>(Resource.Id.Table_Total);

			if (updateTotal.Text != "") {
				string getTax = updateTax.SelectedItem.ToString();
				getTax = getTax.Remove(getTax.Length - 1);

				double tempTax = (Convert.ToDouble(getTax) / 100);
				double tempTotal = Convert.ToDouble (updateTotal.Text.ToString ());
				updateNetto.Text = (tempTotal - (tempTotal * tempTax)).ToString();
			} else {
				updateNetto.Text = null;
			}
		}

		private void updateSaldo()
		{
			Boolean hasAllTotals = true;
			double newSaldo = 0;

			for (int i = 0; i < table.ChildCount; i++)
			{
				View currentRow = table.GetChildAt(i);
				EditText currentTotal = currentRow.FindViewById<EditText>(Resource.Id.Table_Total);

				if (currentTotal.Text == "") {
					hasAllTotals = false;
				} else {
					newSaldo += Convert.ToDouble(currentTotal.Text.ToString());
				}
			}

			saldo.Text = "";

			if (hasAllTotals)
			{
				saldo.Text = newSaldo.ToString();
			}
		}

		private void saveReceipts()
		{
			string year = yearEdit.Text.ToString();
			string month = monthEdit.Text.ToString();
			string day = dayEdit.Text.ToString();
			String name = nameEdit.Text.ToString();

			for (int i = 0; i < table.ChildCount; i++)
			{
				ReceiptEntry r = new ReceiptEntry();

				r.YearValue = year;
				r.MonthValue = month;
				r.DayValue = day;
				r.NameValue = name;

				View currentRow = table.GetChildAt(i);

				EditText tempNumber = currentRow.FindViewById<EditText>(Resource.Id.Table_Number);
				Spinner tempTax = currentRow.FindViewById<Spinner>(Resource.Id.Table_Tax);
				TextView tempNetto = currentRow.FindViewById<TextView>(Resource.Id.Table_Netto);
				EditText tempTotal = currentRow.FindViewById<EditText>(Resource.Id.Table_Total);
				EditText tempPerson = currentRow.FindViewById<EditText>(Resource.Id.Table_People);

				r.NumberValue = Convert.ToInt32 (tempNumber.Text);

				string getTax = tempTax.SelectedItem.ToString();
				getTax = getTax.Remove(getTax.Length - 1);

				r.TaxValue = Convert.ToInt32 (getTax);
				r.NettoValue = Convert.ToDouble(tempNetto.Text);
				r.TotalValue = Convert.ToDouble(tempTotal.Text);
				r.PersonValue = Convert.ToInt32 (tempPerson.Text);

				manager.addReceipt(r);
			}

			Console.WriteLine ("Saved input");
			Toast.MakeText(this, "Success - Saved input", ToastLength.Short).Show();
		}

		private void initTaxSpinner(Spinner spinner)
		{
			List<string> list = new List<string> { "6%", "12%", "25%" };

			ArrayAdapter changeAdapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerItem, list);
			changeAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
			spinner.Adapter = changeAdapter;
		}

		private Boolean validateInput()
		{
			EditText checkYear = FindViewById<EditText>(Resource.Id.Add_YearEdit);
			EditText checkMonth = FindViewById<EditText>(Resource.Id.Add_MonthEdit);
			EditText checkDay = FindViewById<EditText>(Resource.Id.Add_DayEdit);
			EditText checkName = FindViewById<EditText>(Resource.Id.Add_NameEdit);

			if (checkName.Text == "" || checkYear.Text == "" || checkMonth.Text == "" || checkDay.Text == "")  {
				Toast.MakeText(this, "Error - Missing Input in Date and/or Name", ToastLength.Short).Show();
				return false;
			}

			if (!checkDate(checkYear.Text, checkMonth.Text, checkDay.Text)) {
				Toast.MakeText(this, "Error - Invalid Date", ToastLength.Short).Show();
				return false;
			}

			for (int i = 0; i < table.ChildCount; i++)
			{
				View checkRow = table.GetChildAt(i);
				EditText checkNumber = checkRow.FindViewById<EditText>(Resource.Id.Table_Number);
				EditText checkTotal = checkRow.FindViewById<EditText>(Resource.Id.Table_Total);
				EditText checkPerson = checkRow.FindViewById<EditText>(Resource.Id.Table_People);

				if (checkNumber.Text == "" || checkTotal.Text == "" || checkPerson.Text == "") {

					Toast.MakeText(this, "Error - Missing input in Table.", ToastLength.Short).Show();
					return false;
				}
			}

			return true;
		}

		private Boolean checkDate(string year, string month, string day)
		{
			if (year.ToString().Length != 4 || month.ToString().Length < 2 || day.ToString().Length < 2)
			{
				return false;
			}

			int checkMonth = Convert.ToInt32 (month);
			int checkDay = Convert.ToInt32 (day);

			if (checkMonth > 12)
			{
				return false;
			}

			if (checkDay > 31)
			{
				return false;
			}

			if (checkMonth == 4 || checkMonth == 6 || checkMonth == 9 || checkMonth == 11)
			{
				if (checkDay > 30)
				{
					return false;
				}
			}

			if (checkMonth == 2 && checkDay > 29)
			{
				return false;
			}

			return true;
		}

		private void clearView()
		{
			yearEdit.Text = null;
			monthEdit.Text = null;
			dayEdit.Text = null;
			nameEdit.Text = null;
			saldo.Text = null;
			table.RemoveAllViews();
			newTableRow();
		}
	}
}