﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

using SQLite;

namespace KvittoApp
{
	public class ReceiptEntry
	{
		[PrimaryKey, AutoIncrement]
		public int Id { get; private set; }
		public string YearValue { get; set; }
		public string MonthValue { get; set; }
		public string DayValue { get; set; }
		public string NameValue { get; set; }
		public int NumberValue { get; set; }
		public int TaxValue { get; set; }
		public double NettoValue { get; set; }
		public double TotalValue { get; set; }
		public int PersonValue { get; set; }
		public string DescriptionValue { get; set; }
	}
}

