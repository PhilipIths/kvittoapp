using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using SQLite;

namespace KvittoApp
{
	[Activity (Label = "ArchiveActivity", Theme = "@android:style/Theme.NoTitleBar")]			
	public class ArchiveActivity : Activity
	{
		ReceiptManager manager = ReceiptManager.getInstance;

		TextView header;
		TableLayout tableList;
		TextView currentPageText;
		Button nextPageButton1;
		Button nextPageButton2;
		Button prevPageButton1;
		Button prevPageButton2;

		List<ReceiptEntry> list;
		int currentPage = 1;
		int pageStart = 0;
		int pageLength = 10;
		int pageCount = 0;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.Archive);

			header = FindViewById<TextView>(Resource.Id.Archive_Header);
			tableList = FindViewById<TableLayout> (Resource.Id.Archive_TableList);
			currentPageText= FindViewById<TextView> (Resource.Id.Archive_CurrentPageText);
			nextPageButton1 = FindViewById<Button> (Resource.Id.Archive_NextPageButton1);
			nextPageButton2 = FindViewById<Button> (Resource.Id.Archive_NextPageButton2);
			prevPageButton1 = FindViewById<Button> (Resource.Id.Archive_PrevPageButton1);
			prevPageButton2 = FindViewById<Button> (Resource.Id.Archive_PrevPageButton2);

			list = manager.getList();

			int check = 0;
			while (check < list.Count)
			{
				pageCount += 1;
				check += pageLength;
				Console.WriteLine ("PageCount: " + pageCount + " | Check: " + check);
			}

			nextPageButton1.Click += delegate {
				currentPage += 1;
				pageStart += pageLength;
				updateLayout();
			};

			nextPageButton2.Click += delegate {
				currentPage += 2;
				pageStart += (pageLength * 2);
				updateLayout();
			};

			prevPageButton1.Click += delegate {
				currentPage -= 1;
				pageStart -= pageLength;
				updateLayout();
			};

			prevPageButton2.Click += delegate {
				currentPage -= 2;
				pageStart -= (pageLength * 2);
				updateLayout();
			};

			if (list.Count == 0)
			{
				EmptyMode();
			} else {
				updateLayout();
			}
		}

		private void updateLayout()
		{
			currentPageText.Text = currentPage.ToString();

			nextPageButton1.Text = (Convert.ToInt32(currentPageText.Text) + 1).ToString();
			nextPageButton2.Text = (Convert.ToInt32(currentPageText.Text) + 2).ToString();
			prevPageButton1.Text = (Convert.ToInt32 (currentPageText.Text) - 1).ToString();
			prevPageButton2.Text = (Convert.ToInt32 (currentPageText.Text) - 2).ToString();

			if (Convert.ToInt32 (prevPageButton2.Text) > 0)
			{
				prevPageButton1.Visibility = ViewStates.Visible;
				prevPageButton2.Visibility = ViewStates.Visible;
			} else {
				prevPageButton2.Visibility = ViewStates.Gone;
				if (Convert.ToInt32 (prevPageButton1.Text) > 0) {
					prevPageButton1.Visibility = ViewStates.Visible;
				} else {
					prevPageButton1.Visibility = ViewStates.Gone;
				}
			}

			if (Convert.ToInt32 (nextPageButton2.Text) <= pageCount)
			{
				nextPageButton1.Visibility = ViewStates.Visible;
				nextPageButton2.Visibility = ViewStates.Visible;
			} else {
				nextPageButton2.Visibility = ViewStates.Gone;
				if (Convert.ToInt32 (nextPageButton1.Text) <= pageCount) {
					nextPageButton1.Visibility = ViewStates.Visible;
				} else {
					nextPageButton1.Visibility = ViewStates.Gone;
				}
			}

			populateList(pageStart, (pageStart+pageLength));
		}

		private void populateList(int start, int end)
		{
			int checkEnd = end;
			if (checkEnd > list.Count)
			{
				checkEnd = list.Count;
			}

			tableList.RemoveAllViews();

			for (int i = start; i < checkEnd; i++)
			{
				RowInstance rowInfo = new RowInstance();
				rowInfo.row = LayoutInflater.From (this).Inflate (Resource.Drawable.archive_list_style, null, false);
				tableList.AddView(rowInfo.row);

				TextView rowText = rowInfo.row.FindViewById<TextView>(Resource.Id.List_Text);
				Button rowButton = rowInfo.row.FindViewById<Button>(Resource.Id.List_Button);

				rowText.Text = (list[i].YearValue+"/"+list[i].MonthValue+"/"+list[i].DayValue+" | "+list[i].TotalValue+" - "+list[i].NameValue);

				ReceiptEntry rec = list[i];
				int Id = list[i].Id;

				rowText.Click += delegate {
					Intent intent = new Intent(this, typeof(CheckReceipt));
					intent.PutExtra("selectedId", Id);
					StartActivity(intent);
				};

				rowButton.Click += delegate {
					startDialog(rec, rowInfo.row);
				};
			}
		}

		private void startDialog(ReceiptEntry rec, View row)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.SetTitle("Ta bort kvitto");
			builder.SetIconAttribute(Android.Resource.Attribute.AlertDialogIcon);
			builder.SetMessage("Är det säkert att du vill radera detta kvittot?");
			builder.SetPositiveButton("Confirm", (s, e) => {
				manager.deleteReceipt(rec);
				tableList.RemoveView(row);
				if (list.Count() == 0) {
					EmptyMode();
				} else
				{
					if (tableList.ChildCount == 0)
					{
						currentPage -= 1;
						pageCount -= 1;
						pageStart -= pageLength;
						updateLayout();
					}
				}
			});
			builder.SetNegativeButton("Cancel", (s, e) => {});

			builder.Create().Show();
		}

		private void EmptyMode()
		{
			header.Text = "Du har inga kvitton sparade";
			currentPageText.Visibility = ViewStates.Gone;
			nextPageButton1.Visibility = ViewStates.Gone;
			nextPageButton2.Visibility = ViewStates.Gone;
			prevPageButton1.Visibility = ViewStates.Gone;
			prevPageButton2.Visibility = ViewStates.Gone;
		}
	}
}